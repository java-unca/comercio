package excepciones;

public class EncontradoException extends Exception {
    public EncontradoException(String mensaje) {
        super(mensaje);
    }
}
