package modelo;

import persistencia.PersistenciaModel;

public class Cliente implements PersistenciaModel{
    private String cuil;
    private String apellidoNombre;
    private String iva;

    public Cliente(String cuil, String apellidoNombre, String iva){
        this.cuil=cuil;
        this.apellidoNombre=apellidoNombre;
        this.iva=iva;
    }
    /**
     * @return the cuil
     */
    public String getCuil() {
        return cuil;
    }

    /**
     * @param cuil the cuil to set
     */
    public void setCuil(String cuil) {
        this.cuil = cuil;
    }

    /**
     * @return the apellidoNombre
     */
    public String getApellidoNombre() {
        return apellidoNombre;
    }

    /**
     * @param apellidoNombre the apellidoNombre to set
     */
    public void setApellidoNombre(String apellidoNombre) {
        this.apellidoNombre = apellidoNombre;
    }

    /**
     * @return the iva
     */
    public String getIva() {
        return iva;
    }

    /**
     * @param iva the iva to set
     */
    public void setIva(String iva) {
        this.iva = iva;
    }
    
}
