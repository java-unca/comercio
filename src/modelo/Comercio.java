
package modelo;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import persistencia.PersistenciaDAO;
import repositorio.CatalogoProducto;
import repositorio.FicheroCliente;

public class Comercio {

    private CatalogoProducto catalogoProducto;
    private FicheroCliente ficheroCliente;  
    
    private final Properties configuracion;
    
    private static Comercio comercio;
    
    private Comercio(Properties prop){       
        this.configuracion = prop;
    }
   
    public static Comercio instancia(Properties prop){
        if (comercio==null){
            comercio=new Comercio(prop);
            comercio.catalogoProducto = new CatalogoProducto(comercio);
            comercio.ficheroCliente = new FicheroCliente(comercio);
        }
        return comercio;
    }

    public static Comercio instancia(){
        return comercio;
    }

    
    public FicheroCliente getFicheroCliente() {
        return ficheroCliente;
    }
    
    public CatalogoProducto getCatalogoProducto() {
        return catalogoProducto;
    }

    public PersistenciaDAO getClienteDAO() {
        return getModelDAO("ClienteDAO");
    }

    public PersistenciaDAO getCondicionIVADAO() {
        return getModelDAO("CondicionIVADAO");
    }

    public PersistenciaDAO getProductoDAO() {
        return getModelDAO("ProductoDAO");

    }

    public PersistenciaDAO getModelDAO(String clase) {
        String nombreClase = configuracion.getProperty(clase);
        PersistenciaDAO modelo = null;
        try {
            modelo = (PersistenciaDAO)Class.forName(nombreClase).getConstructor().newInstance();
        } catch (Exception ex) {
            Logger.getLogger(Comercio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
}
