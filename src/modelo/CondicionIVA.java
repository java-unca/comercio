package modelo;

import persistencia.PersistenciaModel;

public class CondicionIVA  implements PersistenciaModel{
    private String iva;    

    public CondicionIVA(String iva){
        this.iva=iva;
    }            
    /**
     * @return the iva
     */
    public String getIva() {
        return iva;
    }

    /**
     * @param iva the iva to set
     */
    public void setIva(String iva) {
        this.iva = iva;
    }
}
