package repositorio;

import excepciones.DataAccessException;
import excepciones.EncontradoException;
import excepciones.NoEncontradoException;
import java.util.ArrayList;
import modelo.Cliente;
import modelo.Comercio;
import modelo.CondicionIVA;
import persistencia.PersistenciaDAO;

public class FicheroCliente {

    Comercio comercio;
    
    private PersistenciaDAO clienteDAO;
    private PersistenciaDAO condicionIVADAO;
    
    public FicheroCliente(Comercio comercio){
        this.comercio=comercio;
        this.clienteDAO = comercio.getClienteDAO();
        this.condicionIVADAO = comercio.getCondicionIVADAO();
    }
    
    
    public void agregarCliente(Cliente cliente) throws DataAccessException, EncontradoException{
        clienteDAO.insertar(cliente);
    }
    public void agregarCondicionIva(CondicionIVA condicionIva) throws DataAccessException, EncontradoException{
        condicionIVADAO.insertar(condicionIva);
    }
    public void removerCondicionIva(String condicion) throws DataAccessException, NoEncontradoException{
        condicionIVADAO.eliminar(condicion);
    }
    
    public ArrayList<Cliente> clientes() throws DataAccessException{
        return (ArrayList)clienteDAO.buscarTodos();
    }
    public ArrayList<CondicionIVA> condicionesIVA() throws DataAccessException{
        return (ArrayList)condicionIVADAO.buscarTodos();
    }
    
}
