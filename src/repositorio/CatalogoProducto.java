package repositorio;

import excepciones.DataAccessException;
import excepciones.EncontradoException;
import java.util.ArrayList;
import modelo.Comercio;
import modelo.Producto;
import persistencia.PersistenciaDAO;

public class CatalogoProducto {
    
    Comercio comercio;
    
    private PersistenciaDAO productoDAO; 
    
    public CatalogoProducto(Comercio comercio){
        this.comercio=comercio;
        productoDAO = comercio.getProductoDAO();
    }
        
    public void agregarProducto(Producto producto) throws DataAccessException, EncontradoException{
        productoDAO.insertar(producto);
    }
    public ArrayList<Producto> productos() throws DataAccessException{
        return (ArrayList)productoDAO.buscarTodos();
    }    
}
