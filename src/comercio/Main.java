package comercio;

import ui.Principal;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.swing.JFrame;
import modelo.Comercio;

public class Main {

    public static void main(String[] args) {
        
        String propertiesFilename = "config.properties"; 
        Properties prop = loadProperties(propertiesFilename);
        Comercio comercio = Comercio.instancia(prop);
        
        JFrame aplicacion = new Principal("Gestion de Comercio");
        aplicacion.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        
        Dimension dim_pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dim_cuadro = aplicacion.getSize();
        aplicacion.setLocation((dim_pantalla.width-dim_cuadro.width)/2,(dim_pantalla.height-dim_cuadro.height)/2);        
        
        aplicacion.setVisible(true); 
        
    }
    
   private static Properties loadProperties(String propertiesFilename)
    {
        Properties prop = new Properties();
        ClassLoader loader = Main.class.getClassLoader(); 
        try (InputStream stream = loader.getResourceAsStream(propertiesFilename))
        {
            if (stream == null) {
                throw new FileNotFoundException();
            }
            prop.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }    
}
