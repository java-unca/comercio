package persistencia.array;

import excepciones.DataAccessException;
import excepciones.EncontradoException;
import excepciones.NoEncontradoException;
import java.util.ArrayList;
import java.util.Collection;
import modelo.Cliente;
import persistencia.BaseDeDatosArray;
import persistencia.PersistenciaDAO;
import persistencia.PersistenciaModel;

public class ClienteArray implements PersistenciaDAO {
    @Override
    public void eliminar(String pk) throws DataAccessException, NoEncontradoException {
        Cliente cliente = (Cliente)buscarPorClavePrimaria(pk);
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CLIENTE.ordinal());
        base.remove(cliente);

    }
    public boolean existeCliente(String pk) {
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CLIENTE.ordinal());
        for (PersistenciaModel var : base) {
            Cliente cliente = (Cliente)var;
            if (cliente.getCuil().equals(pk)) {
                return true;
            }
        }
        return false;
    }    
    @Override
    public PersistenciaModel buscarPorClavePrimaria(String pk) throws DataAccessException, NoEncontradoException {
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CLIENTE.ordinal());
        Cliente cliente = null;
            
        for (PersistenciaModel var : base) {
            Cliente clienteBase = (Cliente)var;
            if (clienteBase.getCuil().equals(pk)) {
                cliente = clienteBase;
                break;
            }
        }
        if (cliente == null) {
            throw new NoEncontradoException("No se encuentra el Cliente");
        }                
        return cliente;
    }
    @Override
    public Collection buscarTodos() throws DataAccessException{       
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CLIENTE.ordinal());
        return base;
    }        
    @Override
    public void insertar(PersistenciaModel objetoAPersistir) throws DataAccessException, EncontradoException {
        Cliente cliente=(Cliente)objetoAPersistir;
        if (existeCliente(cliente.getCuil())==false) {
            ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CLIENTE.ordinal());
            base.add(cliente);            
        } else {
            throw new EncontradoException("Cliente existente en ClienteDAO");
        }
    }
    @Override
    public void actualizar(PersistenciaModel objetoAPersistir) throws DataAccessException,NoEncontradoException {
        Cliente cliente=(Cliente)objetoAPersistir;
        Cliente clienteEncontrado = (Cliente)buscarPorClavePrimaria(cliente.getCuil());
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CLIENTE.ordinal());
        base.remove(clienteEncontrado);        
        base.add(cliente);
    }    
}
