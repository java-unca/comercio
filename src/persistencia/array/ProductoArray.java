package persistencia.array;

import excepciones.DataAccessException;
import excepciones.EncontradoException;
import excepciones.NoEncontradoException;
import java.util.ArrayList;
import java.util.Collection;
import modelo.Producto;
import persistencia.BaseDeDatosArray;
import persistencia.PersistenciaDAO;
import persistencia.PersistenciaModel;

public class ProductoArray implements PersistenciaDAO {
    @Override
    public void eliminar(String pk) throws DataAccessException, NoEncontradoException {
        Producto producto = (Producto)buscarPorClavePrimaria(pk);
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.PRODUCTO.ordinal());
        base.remove(producto);

    }
    public boolean existeProducto(String pk) {
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.PRODUCTO.ordinal());
        for (PersistenciaModel var : base) {
            Producto producto = (Producto)var;
            if (producto.getCodigo().equals(pk)) {
                return true;
            }
        }
        return false;
    }        
    @Override
    public PersistenciaModel buscarPorClavePrimaria(String pk) throws DataAccessException, NoEncontradoException {
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.PRODUCTO.ordinal());
        Producto producto = null;
            
        for (PersistenciaModel var : base) {
            Producto productoBase = (Producto)var;
            if (productoBase.getCodigo().equals(pk)) {
                producto = productoBase;
                break;
            }
        }
        if (producto == null) {
            throw new NoEncontradoException("No se encuentra el Producto");
        }                
        return producto;
    }
    @Override
    public Collection buscarTodos() throws DataAccessException{       
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.PRODUCTO.ordinal());
        return base;
    }        
    @Override
    public void insertar(PersistenciaModel objetoAPersistir) throws DataAccessException, EncontradoException {
        Producto producto=(Producto)objetoAPersistir;
        if (existeProducto(producto.getCodigo())==false) {
            ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.PRODUCTO.ordinal());
            base.add(producto);            
        } else {
            throw new EncontradoException("Producto existente en ProductoDAO");
        }
    }
    @Override
    public void actualizar(PersistenciaModel objetoAPersistir) throws DataAccessException,NoEncontradoException {
        Producto producto=(Producto)objetoAPersistir;
        Producto productoEncontrado = (Producto)buscarPorClavePrimaria(producto.getCodigo());
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.PRODUCTO.ordinal());
        base.remove(productoEncontrado);        
        base.add(producto);
    }    
}
