package persistencia.array;

import excepciones.DataAccessException;
import excepciones.EncontradoException;
import excepciones.NoEncontradoException;
import java.util.ArrayList;
import java.util.Collection;
import modelo.CondicionIVA;
import persistencia.BaseDeDatosArray;
import persistencia.PersistenciaDAO;
import persistencia.PersistenciaModel;

public class CondicionIVAArray implements PersistenciaDAO {
    @Override
    public void eliminar(String pk) throws DataAccessException, NoEncontradoException {
        CondicionIVA condicionIVA = (CondicionIVA)buscarPorClavePrimaria(pk);
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CONDICION_IVA.ordinal());
        base.remove(condicionIVA);

    }
    public boolean existeCondicionIVA(String pk) {
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CONDICION_IVA.ordinal());
        for (PersistenciaModel var : base) {
            CondicionIVA condicionIVABase = (CondicionIVA)var;
            if (condicionIVABase.getIva().equals(pk)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public PersistenciaModel buscarPorClavePrimaria(String pk) throws DataAccessException, NoEncontradoException {
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CONDICION_IVA.ordinal());
        CondicionIVA condicionIVA = null;
            
        for (PersistenciaModel var : base) {
            CondicionIVA condicionIVABase = (CondicionIVA)var;
            if (condicionIVABase.getIva().equals(pk)) {
                condicionIVA = condicionIVABase;
                break;
            }
        }
        if (condicionIVA == null) {
            throw new NoEncontradoException("No se encuentra el Condicion de IVA");
        }                
        return condicionIVA;
    }
    @Override
    public Collection buscarTodos() throws DataAccessException{       
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CONDICION_IVA.ordinal());
        return base;
    }        
    @Override
    public void insertar(PersistenciaModel objetoAPersistir) throws DataAccessException, EncontradoException {
        CondicionIVA condicionIVA=(CondicionIVA)objetoAPersistir;
        if (existeCondicionIVA(condicionIVA.getIva())==false) {
            ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CONDICION_IVA.ordinal());
            base.add(condicionIVA);            
        } else {
            throw new EncontradoException("Condicion IVA existente en CondicionIVADAO");
        }
    }
    @Override
    public void actualizar(PersistenciaModel objetoAPersistir) throws DataAccessException,NoEncontradoException {
        CondicionIVA condicionIVA=(CondicionIVA)objetoAPersistir;
        CondicionIVA condicionIVAEncontrada = (CondicionIVA)buscarPorClavePrimaria(condicionIVA.getIva());
        ArrayList<PersistenciaModel> base = BaseDeDatosArray.getInstance(BaseDeDatosArray.Tablas.CONDICION_IVA.ordinal());
        base.remove(condicionIVAEncontrada);        
        base.add(condicionIVA);
    }    
}
