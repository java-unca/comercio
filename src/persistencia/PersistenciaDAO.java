package persistencia;

import excepciones.DataAccessException;
import excepciones.EncontradoException;
import excepciones.NoEncontradoException;
import java.util.Collection;

public interface PersistenciaDAO {
    public void eliminar(String pk) throws DataAccessException, NoEncontradoException;
    public PersistenciaModel buscarPorClavePrimaria(String pk) throws DataAccessException, NoEncontradoException;
    public Collection buscarTodos() throws DataAccessException;
    public void insertar(PersistenciaModel objetoAPersistir) throws DataAccessException, EncontradoException;
    public void actualizar(PersistenciaModel objetoAPersistir) throws DataAccessException,NoEncontradoException;
}
