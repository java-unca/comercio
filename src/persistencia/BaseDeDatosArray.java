package persistencia;

import java.util.ArrayList;
import java.util.List;

public class BaseDeDatosArray {    
    protected static List<ArrayList<PersistenciaModel>> instance = null; 
    public static  ArrayList<PersistenciaModel> getInstance(int indice) {
        if (instance == null) {
            instance = new ArrayList<ArrayList<PersistenciaModel>>();
            instance.add(new ArrayList<PersistenciaModel>());
            instance.add(new ArrayList<PersistenciaModel>());
            instance.add(new ArrayList<PersistenciaModel>());
        }                
        return instance.get(indice);
    }
    public enum Tablas
    {
        CLIENTE, PRODUCTO,CONDICION_IVA
    }    
}