package persistencia.sql;

import excepciones.DataAccessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import modelo.Cliente;
import persistencia.BaseDeDatosSQL;
import persistencia.PersistenciaDAO;
import persistencia.PersistenciaModel;

public class ClienteSQL implements PersistenciaDAO {
    @Override
    public void eliminar(String pk) throws DataAccessException {
        try {
            Connection con = BaseDeDatosSQL.getInstance();
            Statement smt = con.createStatement();
            smt.executeUpdate("Delete from cliente where cuil='"+pk+"'");
            smt.close();
        } catch (Exception e) {
            throw new DataAccessException("Error en ClienteDAO.delete() "+e);
        }
    }
    @Override
    public PersistenciaModel buscarPorClavePrimaria(String pk) throws DataAccessException {
         try {
            Connection con = BaseDeDatosSQL.getInstance();
            Statement smt = con.createStatement();
            ResultSet result = smt.executeQuery("Select * from cliente where cuil='"+pk+"'");            
            Cliente cliente = null;
            while (result.next())  {
                cliente = new Cliente(result.getString("cuil"),result.getString("apellido_nombre"),result.getString("iva"));
            }
            result.close();
            smt.close();
            return cliente;
        } catch (Exception e) {
            throw new DataAccessException("Error en ClienteDAO.findByPK() "+e);
        }
    }
    @Override
    public Collection buscarTodos() throws DataAccessException{       
        try {
            Connection con = BaseDeDatosSQL.getInstance();
            Statement smt = con.createStatement();
            ResultSet result = smt.executeQuery("Select * from Cliente order by apellido_nombre");            
            Cliente cliente = null;
            ArrayList array = new ArrayList();
            while (result.next())  {
                cliente = new Cliente(result.getString("cuil"),result.getString("apellido_nombre"),result.getString("iva"));
                array.add(cliente);
            }
            result.close();
            smt.close();
            return array;
        } catch (Exception e) {
            throw new DataAccessException("Error en CuentaDAO.findAll() "+e);
        }        
    }        
    @Override
    public void insertar(PersistenciaModel objetoAPersistir) throws DataAccessException {
        Cliente cliente=(Cliente)objetoAPersistir;
        try {
            Cliente existe = (Cliente)buscarPorClavePrimaria(cliente.getCuil());
            if (existe!=null) {
                throw new DataAccessException("Cliente existente en ClienteDAO.insert()");
            }
            Connection con = BaseDeDatosSQL.getInstance();
            PreparedStatement smt = con.prepareStatement("Insert into cliente (cuil,apellido_nombre,iva) values (?,?,?)");
            smt.setString(1,cliente.getCuil());
            smt.setString(2,cliente.getApellidoNombre());
            smt.setString(3,cliente.getIva());
            smt.execute();            
        } catch (Exception e) {
            throw new DataAccessException("Error en ClienteDAO.insert() "+e);
        }
    }
    @Override
    public void actualizar(PersistenciaModel objetoAPersistir) throws DataAccessException {
       Cliente cliente=(Cliente)objetoAPersistir;
       try {
            Connection con = BaseDeDatosSQL.getInstance();
            PreparedStatement smt = con.prepareStatement("Update cliente set apellido_nombre=?,iva=? where cuil=?");
            smt.setString(1,cliente.getApellidoNombre());
            smt.setString(2,cliente.getIva());
            smt.setString(3,cliente.getCuil());               
            smt.execute();                        
        } catch (Exception e) {
            throw new DataAccessException("Error en ClienteDAO.update() "+e);
        }
    }    
}
