package persistencia.sql;

import excepciones.DataAccessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import modelo.CondicionIVA;
import persistencia.BaseDeDatosSQL;
import persistencia.PersistenciaDAO;
import persistencia.PersistenciaModel;

public class CondicionIVASQL implements PersistenciaDAO{
    
    @Override
    public void eliminar(String pk) throws DataAccessException {
        try {
            Connection con = BaseDeDatosSQL.getInstance();
            Statement smt = con.createStatement();
            smt.executeUpdate("Delete from condicion_iva where iva='"+pk+"'");
            smt.close();
        } catch (Exception e) {
            throw new DataAccessException("Error en CondicionIVADAO.delete() "+e);
        }
    }
    @Override
    public PersistenciaModel buscarPorClavePrimaria(String pk) throws DataAccessException {
         try {
            Connection con = BaseDeDatosSQL.getInstance();
            Statement smt = con.createStatement();
            ResultSet result = smt.executeQuery("Select * from condicion_iva where iva='"+pk+"'");            
            CondicionIVA condicionIVA = null;
            while (result.next())  {
                condicionIVA = new CondicionIVA(result.getString("iva"));
            }
            result.close();
            smt.close();
            return condicionIVA;
        } catch (Exception e) {
            throw new DataAccessException("Error en CondicionIVADAO.findByPK() "+e);
        }
    }
    @Override
    public Collection buscarTodos() throws DataAccessException{       
        try {
            Connection con = BaseDeDatosSQL.getInstance();
            Statement smt = con.createStatement();
            ResultSet result = smt.executeQuery("Select * from condicion_iva order by iva");            
            CondicionIVA condicionIVA = null;
            ArrayList array = new ArrayList();
            while (result.next())  {
                condicionIVA = new CondicionIVA(result.getString("iva"));
                array.add(condicionIVA);
            }
            result.close();
            smt.close();
            return array;
        } catch (Exception e) {
            throw new DataAccessException("Error en CondicionIVADAO.findAll() "+e);
        }        
    }        
    @Override
    public void insertar(PersistenciaModel objetoAPersistir) throws DataAccessException {
        CondicionIVA condicionIVA=(CondicionIVA)objetoAPersistir;
        try {
            CondicionIVA existe = (CondicionIVA)buscarPorClavePrimaria(condicionIVA.getIva());
            if (existe!=null) {
                throw new DataAccessException("CondicionIVA existente en CondicionIVADAO.insert()");
            }
            Connection con = BaseDeDatosSQL.getInstance();
            PreparedStatement smt = con.prepareStatement("Insert into condicion_iva (iva) values (?)");
            smt.setString(1,condicionIVA.getIva());
            smt.execute();            
        } catch (Exception e) {
            throw new DataAccessException("Error en CondicionIVADAO.insert() "+e);
        }
    }
    @Override
    public void actualizar(PersistenciaModel objetoAPersistir) throws DataAccessException {
       CondicionIVA condicionIVA=(CondicionIVA)objetoAPersistir; 
       try {
            Connection con = BaseDeDatosSQL.getInstance();
            PreparedStatement smt = con.prepareStatement("Update condicion_iva set iva=? where iva=?");
            smt.setString(1,condicionIVA.getIva());
            smt.setString(2,condicionIVA.getIva());
            smt.execute();                        
        } catch (Exception e) {
            throw new DataAccessException("Error en CondicionIVADAO.update() "+e);
        }
    }    
}
