package persistencia.sql;

import excepciones.DataAccessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import modelo.Producto;
import persistencia.BaseDeDatosSQL;
import persistencia.PersistenciaDAO;
import persistencia.PersistenciaModel;

public class ProductoSQL implements PersistenciaDAO{
    @Override
    public void eliminar(String pk) throws DataAccessException {
        try {
            Connection con = BaseDeDatosSQL.getInstance();
            Statement smt = con.createStatement();
            smt.executeUpdate("Delete from producto where codigo='"+pk+"'");
            smt.close();
        } catch (Exception e) {
            throw new DataAccessException("Error en ProductoDAO.delete() "+e);
        }
    }
    @Override
    public PersistenciaModel buscarPorClavePrimaria(String pk) throws DataAccessException {
         try {
            Connection con = BaseDeDatosSQL.getInstance();
            Statement smt = con.createStatement();
            ResultSet result = smt.executeQuery("Select * from producto where codigo='"+pk+"'");            
            Producto producto = null;
            while (result.next())  {
                producto = new Producto(result.getString("codigo"),result.getString("descripcion"));
            }
            result.close();
            smt.close();
            return producto;
        } catch (Exception e) {
            throw new DataAccessException("Error en ProductoDAO.findByPK() "+e);
        }
    }
    @Override
    public Collection buscarTodos() throws DataAccessException{       
        try {
            Connection con = BaseDeDatosSQL.getInstance();
            Statement smt = con.createStatement();
            ResultSet result = smt.executeQuery("Select * from producto order by descripcion");            
            Producto producto = null;
            ArrayList array = new ArrayList();
            while (result.next())  {
                producto = new Producto(result.getString("codigo"),result.getString("descripcion"));
                array.add(producto);
            }
            result.close();
            smt.close();
            return array;
        } catch (Exception e) {
            throw new DataAccessException("Error en ProductoDAO.findAll() "+e);
        }        
    }        
    @Override
    public void insertar(PersistenciaModel objetoAPersistir) throws DataAccessException {
        Producto producto=(Producto)objetoAPersistir;
        try {
            Producto existe = (Producto)buscarPorClavePrimaria(producto.getCodigo());
            if (existe!=null) {
                throw new DataAccessException("Producto existente en ProductoDAO.insert()");
            }
            Connection con = BaseDeDatosSQL.getInstance();
            PreparedStatement smt = con.prepareStatement("Insert into producto (codigo,descripcion) values (?,?)");
            smt.setString(1,producto.getCodigo());
            smt.setString(2,producto.getDescripcion());
            smt.execute();            
        } catch (Exception e) {
            throw new DataAccessException("Error en ProductoDAO.insert() "+e);
        }
    }
    @Override
    public void actualizar(PersistenciaModel objetoAPersistir) throws DataAccessException {
       Producto producto=(Producto)objetoAPersistir;  
       try {
            Connection con = BaseDeDatosSQL.getInstance();
            PreparedStatement smt = con.prepareStatement("Update producto set descripcion=? where codigo=?");
            smt.setString(1,producto.getDescripcion());
            smt.setString(2,producto.getCodigo());
            smt.execute();                        
        } catch (Exception e) {
            throw new DataAccessException("Error en ProductoDAO.update() "+e);
        }
    }    
}
