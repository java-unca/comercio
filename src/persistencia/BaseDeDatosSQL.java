package persistencia;

import java.sql.*;

public class BaseDeDatosSQL {
    protected static Connection instance = null;
    public static Connection getInstance() throws ClassNotFoundException, SQLException {
        if (instance == null) {
            String url = "jdbc:postgresql://127.0.0.1:5432/comercio";  
            String driver = "org.postgresql.Driver";  
            String user = "postgres"; 
            String pass = "postgres";
            Class.forName(driver);
            instance = DriverManager.getConnection(url,user,pass);
        }        
        return instance;
    }
}